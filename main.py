from tqdm import tqdm
import os


filelist = []
filescount = 0
cwd = os.getcwd() #working dict.

print("Aktuelles Verzeichniss:", cwd)


#count files
for root, dirs, files in os.walk(cwd):
    filescount += 1

#search for files
for root, dirs, files in tqdm(os.walk(cwd), total=filescount):
    for name in files:
        if name.endswith((".DS_Store", "desktop.ini")):
            filelist.append(os.path.join(root, name))


#print filelist
if len(filelist) > 0:
    print("--------------------\n", "Datein gefunden:")
    n = 0
    filename = ""
    for filename in filelist:
            n_string = "[" + str(n) + "]"
            print(n_string, filename)
            n += 1
else:
    print("nichts gefunden!")


#Delete files
if len(filelist) > 0:
    loeschen = input("Sollen Dateien gelöscht werden? (ja | nein): ")
    if loeschen == "ja":
            for file in tqdm(filelist):
                    print("deleting:", file)
                    os.remove(file)
    elif loeschen == "nein":
            print("abbruch")
